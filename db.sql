
-- Created from pg_dump

CREATE TABLE public.oauth_access_tokens (
    client_id character varying(80),
    userid character varying(80),
    scope character varying(4000),
    revoked boolean DEFAULT false,
    access_token character varying(80) NOT NULL,
    expires integer
);


CREATE TABLE public.oauth_auth_codes (
    client_id character varying(80),
    user_id character varying(80),
    scope character varying(4000),
    revoked boolean DEFAULT false,
    auth_code character varying(80) NOT NULL,
    expires integer
);

CREATE TABLE public.oauth_clients (
    client_id character varying(80) NOT NULL,
    grant_types character varying(80),
    name character varying(255),
    redirect_uri character varying(2000),
    client_secret character varying(255)
);


CREATE TABLE public.oauth_refresh_tokens (
    revoked boolean DEFAULT false,
    refresh_token character varying(80) NOT NULL,
    access_token character varying(80),
    expires integer
);


CREATE TABLE public.oauth_scopes (
    scope_id character varying(80) NOT NULL,
    description text
);


CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(255),
    password character varying(255),
    isadmin integer DEFAULT 0,
    allowed_grant_types character varying(512) DEFAULT 'authorization_code'::character varying
);


CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);

COPY public.oauth_scopes (scope_id, description) FROM stdin;
upload	Permission to upload photos.
change	Permission to change (e.g. rotate or move) photos.
\.

SELECT pg_catalog.setval('public.users_id_seq', 3, true);



ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (access_token);


ALTER TABLE ONLY public.oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (auth_code);


ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (client_id);


ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (refresh_token);

ALTER TABLE ONLY public.oauth_scopes
    ADD CONSTRAINT oauth_scopes_pkey PRIMARY KEY (scope_id);



--
-- PostgreSQL database dump complete
--

