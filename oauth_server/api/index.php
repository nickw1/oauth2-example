<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


require('vendor/autoload.php');
require_once('../common/AccessTokenRepository.php');

session_start();

$app = new \Slim\App(["settings"=>["displayErrorDetails"=>true]]);

$container = $app->getContainer();

$container['db'] = function() {
    $conn = new PDO("pgsql:host=localhost;dbname=otv", "gisuser");
    $container["conn"] = $conn;
    return $conn;
};

$container['server'] = function($c) use($app) {
    $db = $c->get('db');
    $accessTokenRepository = new AccessTokenRepository($db);
    $publicKeyPath="file:///var/www/html/oauth/oauth_server/auth/public.key";
    $server = new \League\OAuth2\Server\ResourceServer(
                        $accessTokenRepository,
                        $publicKeyPath
                    );
    return $server;
};

$app->add(new \League\OAuth2\Server\Middleware\ResourceServerMiddleware($container->get('server')));


$app->post('/photo/upload', function(Request $req, Response $res, array $args) {
    // oauth_access_token_id
    // oauth_client_id
    // oauth_user_id
    // oauth_scopes
    try {
        if(in_array("upload", $req->getAttribute("oauth_scopes"))) {
            return $res->withJson(["msg"=>"Photo uploaded successfully."]);
        } else {
            return $res->withStatus(401)->withJson(["msg"=>"This access token does not permit an upload operation."]);
        }    
    } catch(Exception $e) {
        return $res->withStatus(500)->withJson(["msg"=>$e->getMessage()]);
    }
});

$app->run();

?>
