<?php

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

session_start();

require('vendor/autoload.php');
require_once('../common/ClientRepository.php');
require_once('../common/AccessTokenRepository.php');
require_once('../common/ScopeRepository.php');
require_once('../common/AuthCodeRepository.php');
require_once('../common/RefreshTokenRepository.php');
require_once('../common/ClientEntity.php');
require_once('../common/UserEntity.php');
require_once('../common/AccessTokenEntity.php');
require_once('../common/ScopeEntity.php');
require_once('../common/AuthCodeEntity.php');
require_once('../common/RefreshTokenEntity.php');

$app = new \Slim\App(["settings"=>["displayErrorDetails"=>true]]);

$container = $app->getContainer();

$container['db'] = function() {
    $conn = new PDO("pgsql:host=localhost;dbname=otv", "gisuser");
    $container["conn"] = $conn;
    return $conn;
};

$container['server'] = function($c) {
    $db = $c->get('db');
    $clientRepository = new ClientRepository($db);
    $accessTokenRepository = new AccessTokenRepository($db);
    $scopeRepository = new ScopeRepository($db);
    $authCodeRepository = new AuthCodeRepository($db);
    $refreshTokenRepository = new RefreshTokenRepository($db);
    $privateKeyPath="file:///var/www/private/private.key";
    $server = new AuthorizationServer($clientRepository,
                        $accessTokenRepository,
                        $scopeRepository,
                        $privateKeyPath,
                        "54fO847PCrIiQBDnmJ9Lb7dXVhh92sQRRq5akc0wRUc=" // base64_encode(random_bytes(32))
                        );
    $server->enableGrantType
            (new AuthCodeGrant($authCodeRepository, $refreshTokenRepository,
                    new \DateInterval('PT10M')), new \DateInterval('PT1H'));
    return $server;
};

$container['view'] = new \Slim\Views\PhpRenderer('views');

$app->get('/authorize', function(Request $req, Response $res, array $args) {
    try {
        $get = $req->getQueryParams();
        $authRequest = $this->server->validateAuthorizationRequest($req);
        // if the user ID exists, the user has authorised
        if(!isset($_SESSION["user_has_authorised"])) {
            // $get contains: response_type="code", client_id, redirect_uri, scope, state
            $qs =  http_build_query($get);
            return $res->withRedirect($this->router->pathFor('userAuthorise')."?oauth_data=1&$qs");
        } else {
            $authRequest->setUser(new UserEntity($this->db, $_SESSION["user_has_authorised"]));
            unset($_SESSION['user_has_authorised']);
            $authRequest->setAuthorizationApproved(true);
            return $this->server->completeAuthorizationRequest($authRequest, $res);
        }
    } catch(OAuthServerException $e) {
        return $e->generateHttpResponse($res);
    } catch(\Exception $e) {
        return $res->withStatus(500)->withBody($e->getMessage());
    }
})->setName('authorize');

$app->post('/access_token',function(Request $req, Response $res, array $args) {
    try {
        $this->server->respondToAccessTokenRequest($req, $res);
    } catch(OAuthServerException $e) {
        return $e->generateHttpResponse($res);
    } catch(\Exception $e) {
        return $res->withStatus(500)->withBody($e->getMessage());
    }
});


$app->get('/login/oauth', function (Request $req, Response $res, array $args) {    
    $this->view->render($res, 'login.phtml', ["router"=>$this->router]);
})->setName('loginOAuth');

$app->post('/login/oauth', function(Request $req, Response $res, array $args) {
    $stmt = $this->db->prepare("SELECT id, password FROM users WHERE username=?");
    $stmt->execute([$_POST["username"]]);
    $row = $stmt->fetch();
    if($row !== false && password_verify($_POST["password"], $row["password"])) {
        $_SESSION["userid"] = $row["id"];    
        return $res->withRedirect($this->router->pathFor('userAuthorise'));
    } else {
        $this->view->render($res, 'login.phtml', ['msg'=>'Invalid login!', 'router'=>$this->router]);
    }
})->setName('processLogin');

$app->get('/userAuthorise', function(Request $req, Response $res, array $args) {
    // store the oauth params in a session so we can send it back when user has authorised
    $get = $req->getQueryParams();
    if(isset($get["oauth_data"])) {
        $fields = ["response_type", "client_id", "redirect_uri", "state"];
        $_SESSION["oauth_data"] = [];
        foreach($fields as $field) {
            $_SESSION["oauth_data"][$field] = $_GET[$field];
        }
        $stmt = $this->db->prepare("SELECT name FROM oauth_clients WHERE client_id=?");
        $stmt->execute([$get["client_id"]]);
        $row = $stmt->fetch();
        $_SESSION['oauth_data']['app_name'] = $row['name'];
    }

    if(!isset($_SESSION['userid'])) {
        return $res->withRedirect($this->router->pathFor('loginOAuth'));
    } else {
        $scopeRepo = new ScopeRepository($this->db);
        $allScopes = $scopeRepo->getAllScopes();
        $this->view->render($res, 'authorise.phtml', ["router"=>$this->router, "allScopes"=>$allScopes]);
    }
})->setName('userAuthorise');

$app->post('/userAuthorise', function (Request $req, Response $res, array $args) {
    if(isset($_SESSION['userid']) && isset($_POST["authorise"]) && $_POST["authorise"]=="Authorise") {
        $_SESSION["oauth_data"]["scope"] = implode(" ", $_POST["scopes"]);
        $qs = http_build_query($_SESSION["oauth_data"]);
        unset($_SESSION["oauth_data"]);
        $_SESSION["user_has_authorised"] = $_SESSION["userid"];
        return $res->withRedirect($this->router->pathFor("authorize")."?$qs");
    } else {
        $this->view->render($res, 'authorise.phtml', ['msg'=>'you did not authorise', "router"=>$this->router]);
    }
})->setName('processAuthorise');

$app->run();

?>
