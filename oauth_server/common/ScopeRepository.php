<?php

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;


class ScopeRepository implements ScopeRepositoryInterface {
    protected $db;
    public function __construct($db) {
        $this->db = $db;
    }
    public function getScopeEntityByIdentifier($scopeId) {
        $stmt = $this->db->prepare("SELECT description FROM oauth_scopes WHERE scope_id=?");
        $stmt->execute([$scopeId]);
        $row = $stmt->fetch();
        return $row!==false ? new ScopeEntity($scopeId, $row["description"]) : null;
    }

    public function finalizeScopes(array $scopes, $gramtType, ClientEntityInterface $clientEntity, $userIdentifier=null) {
        return $scopes;
    }
    
    public function getAllScopes() {
        $stmt = $this->db->prepare("SELECT scope_id,description FROM oauth_scopes");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>
