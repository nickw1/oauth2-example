<?php

use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;

class AuthCodeRepository implements AuthCodeRepositoryInterface {
	protected $db;

	public function __construct($db) {
		$this->db = $db;
	}
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity) {
		$scopes = $authCodeEntity->getScopes();
		$allScopes = [];
		foreach($scopes as $scope) {
			$allScopes[] = $scope->getIdentifier();
		}
		$scopes = implode(" ", $allScopes);
        $stmt = $this->db->prepare("INSERT INTO oauth_auth_codes(auth_code, client_id, user_id, expires, scope) VALUES (?,?,?,?,?)");

        $stmt->execute([$authCodeEntity->getIdentifier(), $authCodeEntity->getClient()->getIdentifier(), $authCodeEntity->getUserIdentifier(), $authCodeEntity->getExpiryDateTime()->getTimestamp(), $scopes]);
    }

    
    public function revokeAuthCode($authCode) {
        $stmt = $this->db->prepare("UPDATE oauth_auth_codes SET revoked=true WHERE auth_code=?");
        $stmt->execute([$authCode]);
    }

    public function isAuthCodeRevoked($authCode) {
        $stmt = $this->db->prepare("SELECT revoked FROM oauth_auth_codes WHERE auth_code=?");
        $stmt->execute([$authCode]);
        $row = $stmt->fetch();
        return $row ? $row["revoked"] : true;
    }
    
    public function getNewAuthCode() {
        return new AuthCodeEntity();
    }
}

?>
