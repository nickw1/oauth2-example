<?php

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface {
	protected $db;
	public function __construct($db) {
		$this->db = $db;
	}

	public function getNewRefreshToken() {
		return new RefreshTokenEntity();
	}

	public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshToken) {
		$stmt = $this->db->prepare("INSERT INTO oauth_refresh_tokens(refresh_token, expires, access_token) VALUES (?,?,?)");
		$stmt->execute([$refreshToken->getIdentifier(), $refreshToken->getExpiryDateTime()->getTimestamp(), $refreshToken->getAccessToken()->getIdentifier()]);
	}

	public function revokeRefreshToken($tokenId) {
		$stmt = $this->db->prepare("UPDATE oauth_refresh_tokens SET revoked=true WHERE refresh_token=?");
		$stmt->execute([$tokenId]);
	}

	public function isRefreshTokenRevoked($tokenId) {
		$stmt = $this->db->prepare("SELECT revoked FROM oauth_refresh_tokens WHERE refresh_token=?");
		$stmt->execute([$tokenId]);
		$row = $stmt->fetch();
		return $row ? $row["revoked"] : true;
	}
}

?>
