<?php
require_once 'vendor/autoload.php';
$provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId' => 'e128815d5f2ad47a729df4fe4c7df495',
    'clientSecret' => '4e0663f5b002802353d557ebdf1350529c6815381417be515ace100dd6efced5',
    'redirectUri' => 'http://localhost/oauth/oauth_client/',
    'urlAuthorize' => 'http://localhost/oauth/oauth_server/auth/authorize',
    'scope' => 'all',
    'urlAccessToken' => 'http://localhost/oauth/oauth_server/auth/access_token',
    'urlResourceOwnerDetails' => 'http://localhost/oauth/oauth_server/api'
]);

session_start();

if(!isset($_GET["code"])) {
    $authUrl = $provider->getAuthorizationUrl();
    $_SESSION['state'] = $provider->getState();
    header("Location: $authUrl");
} elseif(empty($_GET['state']) || (isset($_SESSION['state']) && $_GET['state']!=$_SESSION['state'])) {
    unset($_SESSION['state']);
    echo "Possible security violation detected - quitting";
} else {

?>
<!DOCTYPE html>
<html>
<head>
<style>
body {
    background-color: #ffffc0;
    font-family: helvetica, arial, sans-serif;
}
</style>
</head>
<body>
<h1>Hampshire Panos</h1>

<p>This is an example OAuth Client, using the example provider
Open Panos.</p>

<?php
echo "<p>We have an authorisation code.</p>";
try {
    if(!isset($_SESSION["accessToken"])) {
        echo "No saved access token, getting one <br />";
        $accessToken = $provider->getAccessToken('authorization_code',
            ["code"=>$_GET["code"]]);
        $_SESSION["accessToken"] = $accessToken;
    } else {
        echo "We already have an access token, using that<br />";
        $accessToken = $_SESSION["accessToken"];
    }
    echo "Has access token expired? ".($accessToken->hasExpired() ? 'yes': 'no')."<br />";

    $request = $provider->getAuthenticatedRequest(
        'POST',
        'http://localhost/oauth/oauth_server/api/photo/upload',
        $accessToken
    );
    echo "<h2>Making a request to the Open Panos API</h2>";
    echo "<p>User needs to have granted upload permission for it to work, i.e. token needs 'upload' scope.</p>";
    $client = new GuzzleHttp\Client();
    $response = json_decode((string)$client->send($request)->getBody());
    echo "Response from API: <strong>{$response->msg}</strong>";
} catch(\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
    echo "Exception: {$e->getMessage()}";
} catch(GuzzleHttp\Exception\ClientException $e) {
    $status = $e->getResponse()->getStatusCode();
	echo $status==401 ? "This access token does not grant upload permission" :"HTTP error $status";
}
?>


</body>
</html>
<?php
}
?>
